DROP
  TABLE stock IF EXISTS;


CREATE TABLE stock(
  id SERIAL NOT NULL,
  price NUMERIC(19, 5),
  change_point NUMERIC(19, 5),
  change_percentage NUMERIC(19, 5),
  total_vol VARCHAR(255),
  market VARCHAR(255),
  insert_date TIMESTAMP,
  primary key (id)
);
INSERT INTO stock ( id,price, change_point, change_percentage, total_vol, market, insert_date)
                    VALUES (90000,  177.25, 1.23, 2.03, '20.44M', 'TSLA', TO_DATE('12/01/2021', 'DD/MM/YYYY'));
INSERT INTO stock ( id,price, change_point, change_percentage, total_vol, market, insert_date)
                    VALUES ( 90001, 277.25, 1.21, 2.03, '21.44M', 'TSLA', TO_DATE('11/01/2021', 'DD/MM/YYYY'));
 INSERT INTO stock ( id,price, change_point, change_percentage, total_vol, market, insert_date)
                    VALUES (90002,  317.25, 1.33, 2.03, '22.44M', 'TSLA', TO_DATE('11/01/2021', 'DD/MM/YYYY'));
INSERT INTO stock ( id,price, change_point, change_percentage, total_vol, market, insert_date)
                    VALUES (90003,  427.25, 1.23, 2.03, '23.44M', 'TSLA', TO_DATE('11/01/2021', 'DD/MM/YYYY'));
INSERT INTO stock ( id,price, change_point, change_percentage, total_vol, market, insert_date)
                    VALUES ( 90004, 537.25, 1.23, 2.03, '24.44M', 'TSLA', TO_DATE('11/01/2021', 'DD/MM/YYYY'));
INSERT INTO stock ( id,price, change_point, change_percentage, total_vol, market, insert_date)
                    VALUES (90005,  147.25, 1.23, 2.03, '21.44M', 'TSLA', TO_DATE('11/01/2021', 'DD/MM/YYYY'));
INSERT INTO stock ( id,price, change_point, change_percentage, total_vol, market, insert_date)
                    VALUES ( 90006, 247.25, 6.23, 2.03, '25.44M', 'TSLA', TO_DATE('11/01/2021', 'DD/MM/YYYY'));
INSERT INTO stock ( id,price, change_point, change_percentage, total_vol, market, insert_date)
                    VALUES ( 90007, 347.25, 1.23, 2.03, '21.44M', 'TSLA', TO_DATE('11/01/2021', 'DD/MM/YYYY'));
INSERT INTO stock ( id,price, change_point, change_percentage, total_vol, market, insert_date)
                    VALUES ( 90008, 447.25, 7.23, 2.03, '21.44M', 'TSLA', TO_DATE('11/01/2021', 'DD/MM/YYYY'));
INSERT INTO stock ( id,price, change_point, change_percentage, total_vol, market, insert_date)
                    VALUES ( 90009, 541.25, 0.23, 2.03, '26.44M', 'TSLA', TO_DATE('11/01/2021', 'DD/MM/YYYY'));
INSERT INTO stock ( id,price, change_point, change_percentage, total_vol, market, insert_date)
                    VALUES (90100,  112.25, 9.23, 2.03, '31.44M', 'TSLA', TO_DATE('11/01/2021', 'DD/MM/YYYY'));
INSERT INTO stock ( id,price, change_point, change_percentage, total_vol, market, insert_date)
                    VALUES (90011,  213.25, 1.23, 2.03, '41.44M', 'TSLA', TO_DATE('11/01/2021', 'DD/MM/YYYY'));
INSERT INTO stock ( id,price, change_point, change_percentage, total_vol, market, insert_date)
                    VALUES ( 90012, 317.25, 1.23, 2.03, '51.44M', 'TSLA', TO_DATE('11/01/2021', 'DD/MM/YYYY'));
INSERT INTO stock ( id,price, change_point, change_percentage, total_vol, market, insert_date)
                    VALUES ( 90013, 417.25, 1.23, 2.03, '61.44M', 'TSLA', TO_DATE('11/01/2021', 'DD/MM/YYYY'));
INSERT INTO stock ( id,price, change_point, change_percentage, total_vol, market, insert_date)
                    VALUES (90015,  517.25, 1.23, 2.03, '21.44M', 'TSLA', TO_DATE('11/01/2021', 'DD/MM/YYYY'));
INSERT INTO stock ( id,price, change_point, change_percentage, total_vol, market, insert_date)
                    VALUES ( 90016, 107.25, 1.23, 2.03, '21.44M', 'TSLA', TO_DATE('11/01/2021', 'DD/MM/YYYY'));
INSERT INTO stock ( id,price, change_point, change_percentage, total_vol, market, insert_date)
                    VALUES ( 90017, 207.25, 1.23, 2.03, '21.44M', 'TSLA', TO_DATE('11/01/2021', 'DD/MM/YYYY'));
INSERT INTO stock ( id,price, change_point, change_percentage, total_vol, market, insert_date)
                    VALUES ( 90018, 306.25, 1.23, 2.03, '21.44M', 'TSLA', TO_DATE('11/02/2021', 'DD/MM/YYYY'));
