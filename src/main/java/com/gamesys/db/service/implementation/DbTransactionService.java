package com.gamesys.db.service.implementation;

import com.gamesys.controller.model.StockResponse;
import com.gamesys.db.entity.StockEntity;
import com.gamesys.db.mapper.StockMapper;
import com.gamesys.db.service.IDbTransactionService;
import com.gamesys.domain.mapper.StocksResponseMapper;
import com.gamesys.external.model.Stock;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DbTransactionService implements IDbTransactionService {

    private final StocksResponseMapper stocksResponseMapper;
    private final StockRepository repository;
    private final StockMapper stockMapper;

    public DbTransactionService(StocksResponseMapper stocksResponseMapper, StockRepository repository, StockMapper stockMapper) {
        this.stocksResponseMapper = stocksResponseMapper;
        this.repository = repository;
        this.stockMapper = stockMapper;
    }

    @Override
    public List<StockResponse> getStocks() {
        List<StockEntity> stocks = repository.findAllStockEntities(10, 0);
        return stocksResponseMapper.map(stocks);
    }

    @Override
    public List<StockResponse> getStocksByMarket(String market) {
        List<StockEntity> stocks = repository.findAllStocksByMarket(market, 10, 0);
        return stocksResponseMapper.map(stocks);
    }

    @Override
    public void save(Stock stock, String market) {

        StockEntity entity = stockMapper.map(stock, market);
        if (entity != null) {
            int count = repository.findStocksByPrice(entity.getPrice());
            if (count == 0)
                repository.save(entity);
        }
    }

}
