package com.gamesys.db.service.implementation;

import com.gamesys.db.entity.StockEntity;
import com.gamesys.db.repository.IStockRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class StockRepository implements IStockRepository {
    private final JdbcTemplate jdbcTemplate;

    public StockRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    @Transactional
    public void save(StockEntity entity) {
        String sql = "INSERT INTO stock ( price, change_point, change_percentage, total_vol, market, insert_date)" +
                "  VALUES (  ?, ?, ?, ?, ?, ?)";
        jdbcTemplate.update(
                sql,
                entity.getPrice(),
                entity.getChangePoint(),
                entity.getChangePercentage(),
                entity.getTotalVol(),
                entity.getMarket(),
                entity.getInsertDate()
        );
    }

    @Override
    @Transactional(readOnly = true)
    public List<StockEntity> findAllStockEntities(Integer limit, Integer offset) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT * FROM stock").append(" ORDER BY insert_date desc ").append("LIMIT ").append(limit).append(" OFFSET ").append(offset);
        return jdbcTemplate.query(
                sql.toString(),
                (rs, rowNum) -> new StockEntity(rs.getInt("id"),
                        rs.getDouble("price"),
                        rs.getDouble("change_point"),
                        rs.getDouble("change_percentage"),
                        rs.getString("total_vol"),
                        rs.getTimestamp("insert_date"),
                        rs.getString("market"))
        );
    }


    @Override
    @Transactional(readOnly = true)
    public Integer findStocksByPrice(Double price) {
        String sql = "SELECT count(*) FROM stock WHERE price = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{price}, Integer.class);
    }

    @Override
    @Transactional(readOnly = true)
    public List<StockEntity> findAllStocksByMarket(String market, Integer limit, Integer offset) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT * FROM stock WHERE market = ?").append(" ORDER BY insert_date desc ").append(" LIMIT ").append(limit).append(" OFFSET ").append(offset);
        return jdbcTemplate.query(sql.toString(), new Object[]{market}, (rs, rowNum) ->
                new StockEntity(rs.getInt("id"),
                        rs.getDouble("price"),
                        rs.getDouble("change_point"),
                        rs.getDouble("change_percentage"),
                        rs.getString("total_vol"),
                        rs.getTimestamp("insert_date"),
                        rs.getString("market")));
    }

}
