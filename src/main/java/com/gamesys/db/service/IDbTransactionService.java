package com.gamesys.db.service;

import com.gamesys.controller.model.StockResponse;
import com.gamesys.external.model.Stock;

import java.util.List;

public interface IDbTransactionService {
    List<StockResponse> getStocks();

    List<StockResponse> getStocksByMarket(String market);

    void save(Stock stock, String market);
}
