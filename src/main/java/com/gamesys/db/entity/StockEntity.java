package com.gamesys.db.entity;

import java.sql.Timestamp;

public class StockEntity {
    private Integer id;
    private Double price;
    private Double changePoint;
    private Double changePercentage;
    private String totalVol;
    private Timestamp insertDate;
    private String market;

    public StockEntity() {
    }

    public StockEntity(Integer id, Double price, Double changePoint, Double changePercentage, String totalVol, Timestamp insertDate, String market) {
        this.id = id;
        this.price = price;
        this.changePoint = changePoint;
        this.changePercentage = changePercentage;
        this.totalVol = totalVol;
        this.insertDate = insertDate;
        this.market = market;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getChangePoint() {
        return changePoint;
    }

    public void setChangePoint(Double changePoint) {
        this.changePoint = changePoint;
    }

    public Double getChangePercentage() {
        return changePercentage;
    }

    public void setChangePercentage(Double changePercentage) {
        this.changePercentage = changePercentage;
    }

    public String getTotalVol() {
        return totalVol;
    }

    public void setTotalVol(String totalVol) {
        this.totalVol = totalVol;
    }

    public Timestamp getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Timestamp insertDate) {
        this.insertDate = insertDate;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }
}
