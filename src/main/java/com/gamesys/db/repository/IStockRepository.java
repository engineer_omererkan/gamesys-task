package com.gamesys.db.repository;

import com.gamesys.db.entity.StockEntity;

import java.util.List;

public interface IStockRepository {
    void save(StockEntity stockEntity);

    List<StockEntity> findAllStockEntities(Integer limit, Integer offset);

    List<StockEntity> findAllStocksByMarket(String market, Integer limit, Integer offset);

    Integer findStocksByPrice(Double price);

}
