package com.gamesys.db.mapper;

import com.gamesys.db.entity.StockEntity;
import com.gamesys.domain.mapper.IMapper;
import com.gamesys.external.model.Stock;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;

@Service
public class StockMapper implements IMapper<StockEntity, Stock> {

    @Override
    public StockEntity map(Stock model, Object... params) {
        StockEntity entity = null;
        if (model != null) {
            entity = new StockEntity();
            String market = (String) params[0];
            entity.setChangePoint(model.getChangePoint() * 2);
            entity.setChangePercentage(model.getChangePercentage());
            entity.setPrice(model.getPrice());
            entity.setMarket(market);
            entity.setTotalVol(model.getTotalVol());
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            entity.setInsertDate(timestamp);
            entity.setPrice(model.getPrice());
        }
        return entity;
    }
}
