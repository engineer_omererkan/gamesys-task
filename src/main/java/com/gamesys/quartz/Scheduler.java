package com.gamesys.quartz;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.gamesys.db.service.implementation.DbTransactionService;
import com.gamesys.external.model.Stock;
import com.gamesys.external.service.implementation.StonksService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class Scheduler {
    private static final Logger logger = LoggerFactory.getLogger(Scheduler.class);

    @Value("${stonks.market}")
    private String STONKS_MARKET;

    @Autowired
    private StonksService stockRateService;
    @Autowired
    private DbTransactionService transactionService;

    @Scheduled(cron = "*/1 * * * * ?")
    public void cronJobSch() throws JsonProcessingException {
        logger.info("Job is started");
        Stock stock = stockRateService.stockRate(STONKS_MARKET);
        transactionService.save(stock, STONKS_MARKET);
    }
}