package com.gamesys.controller;

import com.gamesys.controller.model.ErrorResponse;
import com.gamesys.domain.exception.JsonParseError;
import com.gamesys.domain.exception.RealStonksServerError;
import com.gamesys.domain.utils.LoggingHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.UUID;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@RestControllerAdvice
public class RestErrorHandler {
    static Logger logger = LoggerFactory.getLogger(RestErrorHandler.class);

    @ExceptionHandler(RealStonksServerError.class)
    @ResponseStatus(INTERNAL_SERVER_ERROR)
    public ErrorResponse handleProviderError(RealStonksServerError ex) {
        String errorId = UUID.randomUUID().toString();
        String result = LoggingHelper.prepareErrorLog(ex.getCode(), ex.getMessage(), errorId, ex.getStackTrace().toString());
        logger.error(result);
        return new ErrorResponse(ex.getCode(), ex.getMessage());
    }

    @ExceptionHandler(JsonParseError.class)
    @ResponseStatus(INTERNAL_SERVER_ERROR)
    public ErrorResponse handleJsonParseError(JsonParseError ex) {
        String errorId = UUID.randomUUID().toString();
        String result = LoggingHelper.prepareErrorLog(ex.getCode(), ex.getMessage(), errorId, ex.getStackTrace().toString());
        logger.error(result);
        return new ErrorResponse(ex.getCode(), ex.getMessage());
    }

}
