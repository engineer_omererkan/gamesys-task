package com.gamesys.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.gamesys.controller.model.StockResponse;
import com.gamesys.domain.service.implementation.EntryService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class StockController {
    private final EntryService entryService;

    public StockController(EntryService entryService) {
        this.entryService = entryService;
    }

    @GetMapping("/api/all/stocks")
    public List<StockResponse> stocks() {

        return entryService.getStocksRate();
    }

    @GetMapping("/api/market/stocks")
    public List<StockResponse> marketStocks(@RequestParam("market") String market) {
        return entryService.getMarketStocksRate(market);
    }

    @GetMapping("/api/stock/rate")
    public StockResponse marketStock(@RequestParam("market") String market) throws JsonProcessingException {
        return entryService.stockRate(market);
    }
}
