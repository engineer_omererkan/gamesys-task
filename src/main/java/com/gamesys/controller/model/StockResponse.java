package com.gamesys.controller.model;

import java.util.Date;

public class StockResponse {
    private Double price;
    private Double changePoint;
    private Double changePercentage;
    private String totalVol;
    private Date insertDate;
    private String market;

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getChangePoint() {
        return changePoint;
    }

    public void setChangePoint(Double changePoint) {
        this.changePoint = changePoint;
    }

    public Double getChangePercentage() {
        return changePercentage;
    }

    public void setChangePercentage(Double changePercentage) {
        this.changePercentage = changePercentage;
    }

    public String getTotalVol() {
        return totalVol;
    }

    public void setTotalVol(String totalVol) {
        this.totalVol = totalVol;
    }

    public Date getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Date insertDate) {
        this.insertDate = insertDate;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }
}
