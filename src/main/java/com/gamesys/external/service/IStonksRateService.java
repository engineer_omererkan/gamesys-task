package com.gamesys.external.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.gamesys.external.model.Stock;

public interface IStonksRateService {
    Stock stockRate(String market) throws JsonProcessingException;
}
