package com.gamesys.external.service.implementation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.gamesys.domain.utils.RestClient;
import com.gamesys.external.mapper.StockJsonMapper;
import com.gamesys.external.model.Stock;
import com.gamesys.external.service.IStonksRateService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class StonksService implements IStonksRateService {

    @Value("${stonks.url}")
    private String STONKS_URL;
    @Value("${stonks.url}")
    private String STONKS_MARKET;

    private final RestClient restClient;
    private final StockJsonMapper stockJsonMapper;

    public StonksService(RestClient restClient, StockJsonMapper stockJsonMapper) {
        this.restClient = restClient;
        this.stockJsonMapper = stockJsonMapper;
    }

    @Override
    public Stock stockRate(String market) throws JsonProcessingException {
        String stonksResponse = restClient.exchange(STONKS_URL + "/" + market, String.class);
        return stockJsonMapper.map(stonksResponse);
    }
}