package com.gamesys.external.model;

public class Stock {
    private Double price;
    private Double changePoint;
    private Double changePercentage;
    private String totalVol;

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getChangePoint() {
        return changePoint;
    }

    public void setChangePoint(Double changePoint) {
        this.changePoint = changePoint;
    }

    public Double getChangePercentage() {
        return changePercentage;
    }

    public void setChangePercentage(Double changePercentage) {
        this.changePercentage = changePercentage;
    }

    public String getTotalVol() {
        return totalVol;
    }

    public void setTotalVol(String totalVol) {
        this.totalVol = totalVol;
    }
}
