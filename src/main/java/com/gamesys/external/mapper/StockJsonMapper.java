package com.gamesys.external.mapper;

import com.gamesys.domain.exception.JsonParseError;
import com.gamesys.domain.mapper.IMapper;
import com.gamesys.external.model.Stock;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import static com.gamesys.domain.enumerator.Error.JSON_PARSE_EXCEPTION;

@Service
public class StockJsonMapper implements IMapper<Stock, String> {
    @Override
    public Stock map(String model, Object... params) {
        if (StringUtils.hasLength(model)) {
            try {
                Stock stock = new Stock();
                model = model.replaceAll("\\\\", "");
                JSONObject json = new JSONObject(model);
                stock.setTotalVol(json.getString("total_vol"));
                stock.setPrice(json.getDouble("price"));
                stock.setChangePoint(json.getDouble("change_point"));
                stock.setChangePercentage(json.getDouble("change_percentage"));
                return stock;
            } catch (Exception e) {
                throw new JsonParseError(JSON_PARSE_EXCEPTION.getCode(), JSON_PARSE_EXCEPTION.getMessage());
            }
        }
        return null;
    }
}
