package com.gamesys.domain.mapper;

public interface IMapper<T, S> {
    T map(S model, Object... params);
}