package com.gamesys.domain.mapper;

import com.gamesys.controller.model.StockResponse;
import com.gamesys.external.model.Stock;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class StockResponseMapper implements IMapper<StockResponse, Stock> {
    @Override
    public StockResponse map(Stock model, Object... params) {
        StockResponse stockResponse = new StockResponse();
        if (model != null) {
            String market = (String) params[0];
            stockResponse.setChangePoint(model.getChangePoint());
            stockResponse.setChangePercentage(model.getChangePercentage());
            stockResponse.setInsertDate(new Date());
            stockResponse.setPrice(model.getPrice());
            stockResponse.setTotalVol(model.getTotalVol());
            stockResponse.setMarket(market);

        }
        return stockResponse;
    }

}
