package com.gamesys.domain.mapper;

import com.gamesys.controller.model.StockResponse;
import com.gamesys.db.entity.StockEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class StocksResponseMapper implements IMapper<List<StockResponse>, List<StockEntity>> {
    @Override
    public List<StockResponse> map(List<StockEntity> model, Object... params) {
        return model.stream().map(i -> {
            StockResponse response = new StockResponse();
            response.setMarket(i.getMarket());
            response.setPrice(i.getPrice());
            response.setTotalVol(i.getTotalVol());
            response.setInsertDate(i.getInsertDate());
            response.setChangePercentage(i.getChangePercentage());
            response.setChangePoint(i.getChangePoint());
            return response;
        }).collect(Collectors.toList());
    }
}
