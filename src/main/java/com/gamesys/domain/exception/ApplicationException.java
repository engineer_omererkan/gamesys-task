package com.gamesys.domain.exception;


import static java.text.MessageFormat.format;

public class ApplicationException extends RuntimeException {
    private static final long serialVersionUID = -4445077739033521957L;

    final String code;
    final String description;

    public ApplicationException(String code, String description) {
        super(message(description));
        this.code = code;
        this.description = description;
    }

    public ApplicationException(String code, String description, Throwable ex) {
        super(message(description), ex);
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    static String message(String description) {
        return format("{0}", description);
    }
}
