package com.gamesys.domain.exception;

public class JsonParseError extends ApplicationException {

    public JsonParseError(String code, String description, Throwable ex) {
        super(code, description, ex);
    }

    public JsonParseError(String code, String message) {
        super(code, message);
    }
}
