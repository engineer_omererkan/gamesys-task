package com.gamesys.domain.exception;

public class RealStonksServerError extends ApplicationException {

    public RealStonksServerError(String code, String description, Throwable ex) {
        super(code, description, ex);
    }

    public RealStonksServerError(String code, String message) {
        super(code, message);
    }
}
