package com.gamesys.domain.utils;

import com.gamesys.domain.enumerator.Error;
import com.gamesys.domain.exception.RealStonksServerError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class RestClient {
    private static final Logger logger = LoggerFactory.getLogger(RestClient.class);

    @Value("${stonks.key}")
    private String STONKS_KEY;
    @Value("${stonks.host}")
    private String STONKS_HOST;
    @Value("${stonks.query.string}")
    private String STONKS_QUERY_STRING;

    final RestTemplate restTemplate;

    public RestClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public <T> T exchange(String url, Class<T> objectType) {
        try {
//            List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
//            messageConverters.add(new StringHttpMessageConverter());
//            restTemplate.setMessageConverters(messageConverters);

            List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
            MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();

            converter.setSupportedMediaTypes(Collections.singletonList(MediaType.ALL));
            messageConverters.add(converter);
            restTemplate.setMessageConverters(messageConverters);
            HttpHeaders headers = new HttpHeaders();
            headers.add("x-rapidapi-key", STONKS_KEY);
            headers.add("x-rapidapi-host", STONKS_HOST);
            headers.add("useQueryString", STONKS_QUERY_STRING);


            HttpEntity<String> data = new HttpEntity<>(headers);
            final ResponseEntity<T> response = restTemplate.exchange(url, HttpMethod.GET, data, objectType);
            response.getBody();
            logger.info("Success Response from External Api");

            return response.getBody();
        } catch (Exception ex) {
            throw new RealStonksServerError(Error.EXTERNAL_SERVER_EXCEPTION.getCode(), Error.EXTERNAL_SERVER_EXCEPTION.getMessage());
        }
    }

}