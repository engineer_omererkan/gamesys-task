package com.gamesys.domain.utils;

import static java.text.MessageFormat.format;

public class LoggingHelper {
    private LoggingHelper() {
    }

    public static String prepareErrorLog(String errorCode, String message, String errorId, String errorStack) {
        return format("Mail Provider Exception: {0} \nCode: {1} \nMessage:{2} \nErrorStack:{3}", errorId, errorCode, message, errorId, errorStack);
    }

}
