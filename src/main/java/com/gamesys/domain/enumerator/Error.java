package com.gamesys.domain.enumerator;

public enum Error {
    EXTERNAL_SERVER_EXCEPTION("01", "External Server error"),
    JSON_PARSE_EXCEPTION("02", "Json Parse Exception");

    private final String code;
    private final String message;

    Error(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
