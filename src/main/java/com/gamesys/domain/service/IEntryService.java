package com.gamesys.domain.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.gamesys.controller.model.StockResponse;

import java.util.List;

public interface IEntryService {
    List<StockResponse> getMarketStocksRate(String market);
    List<StockResponse> getStocksRate();

    StockResponse stockRate(String market) throws JsonProcessingException;
}
