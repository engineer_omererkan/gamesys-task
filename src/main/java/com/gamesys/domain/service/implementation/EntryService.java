package com.gamesys.domain.service.implementation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.gamesys.controller.model.StockResponse;
import com.gamesys.db.service.implementation.DbTransactionService;
import com.gamesys.domain.mapper.StockResponseMapper;
import com.gamesys.domain.service.IEntryService;
import com.gamesys.external.model.Stock;
import com.gamesys.external.service.implementation.StonksService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EntryService implements IEntryService {

    private final DbTransactionService stockRateTransactionService;
    private final StonksService stockRateService;
    private final StockResponseMapper stockResponseMapper;

    public EntryService(DbTransactionService stockRateTransactionService, StonksService stockRateService, StockResponseMapper stockResponseMapper) {
        this.stockRateTransactionService = stockRateTransactionService;
        this.stockRateService = stockRateService;
        this.stockResponseMapper = stockResponseMapper;
    }

    public List<StockResponse> getStocksRate() {
        return stockRateTransactionService.getStocks();
    }

    public List<StockResponse> getMarketStocksRate(String market) {
        return stockRateTransactionService.getStocksByMarket(market);
    }

    public StockResponse stockRate(String market) throws JsonProcessingException {
        Stock stock = stockRateService.stockRate(market);
        return stockResponseMapper.map(stock, market);
    }
}
