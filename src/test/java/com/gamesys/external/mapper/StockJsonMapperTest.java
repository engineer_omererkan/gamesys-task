package com.gamesys.external.mapper;

import com.gamesys.BaseTest;
import com.gamesys.external.model.Stock;
import org.junit.Test;
import org.mockito.InjectMocks;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class StockJsonMapperTest extends BaseTest {
    @InjectMocks
    private StockJsonMapper mapper;

    @Test
    public void testMapperSuccess() {
        // given
        String stockModel = "{\"price\": 677.25, \"change_point\": 0.23, \"change_percentage\": 0.03, \"total_vol\": \"21.44M\"}";
        Stock stock = getStock();
        // when
        Stock response = mapper.map(stockModel);
        // then
        assertNotNull(response);
        assertEquals(stock.getTotalVol(), response.getTotalVol());
        assertEquals(stock.getPrice(), response.getPrice());
        assertEquals(stock.getChangePoint(), response.getChangePoint());
        assertEquals(stock.getChangePercentage(), response.getChangePercentage());
    }
    private Stock getStock() {
        Stock stock = new Stock();
        stock.setChangePercentage(0.03);
        stock.setChangePoint(0.23);
        stock.setPrice(677.25);
        stock.setTotalVol("21.44M");
        return stock;

    }
}
