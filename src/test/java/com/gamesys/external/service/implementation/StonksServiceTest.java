package com.gamesys.external.service.implementation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.gamesys.BaseTest;
import com.gamesys.domain.utils.RestClient;
import com.gamesys.external.mapper.StockJsonMapper;
import com.gamesys.external.model.Stock;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

public class StonksServiceTest extends BaseTest {
    @Mock
    private RestClient restClient;
    @Mock
    private StockJsonMapper stockJsonMapper;

    @InjectMocks
    private StonksService stonksService;

    @Before
    public void before() {
        String STONKS_URL = "https://test.p.rapidapi.com";
        ReflectionTestUtils.setField(stonksService, "STONKS_URL", STONKS_URL);
        String STONKS_MARKET = "TSLA";
        ReflectionTestUtils.setField(stonksService, "STONKS_MARKET", STONKS_MARKET);
    }

    @Test
    public void testStockRateSuccess() throws JsonProcessingException {
        // given
        String stonksResponse = "{\"price\": 681.02, \"change_point\": 10.05, \"change_percentage\": 1.5, \"total_vol\": \"29.43M\"}";
        Stock stock = getStock();
        when(restClient.exchange(Mockito.any(), Mockito.eq(String.class))).thenReturn(stonksResponse);
        when(stockJsonMapper.map(Mockito.anyString())).thenReturn(stock);
        // when
        Stock response = stonksService.stockRate("TSLA");
        // then
        assertNotNull(response);
        assertEquals(stock.getChangePercentage(), response.getChangePercentage());
        assertEquals(stock.getChangePoint(), response.getChangePoint());
        assertEquals(stock.getPrice(), response.getPrice());
        assertEquals(stock.getTotalVol(), response.getTotalVol());
    }

    private Stock getStock() {
        Stock stock = new Stock();
        stock.setChangePercentage(1.5);
        stock.setChangePoint(10.05);
        stock.setPrice(681.02);
        stock.setTotalVol("29.43M");
        return stock;
    }
}
