package com.gamesys.db.mapper;

import com.gamesys.BaseTest;
import com.gamesys.db.entity.StockEntity;
import com.gamesys.external.model.Stock;
import org.junit.Test;
import org.mockito.InjectMocks;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class StockMapperTest extends BaseTest {


    @InjectMocks
    private StockMapper mapper;

    @Test
    public void testMapperSuccess() {
        // given
        Stock stock = getStock();
        StockEntity entity = getStockEntity();
        // when
        StockEntity response = mapper.map(stock, "TSLA");
        // then
        assertNotNull(response);
        assertEquals(entity.getTotalVol(), response.getTotalVol());
        assertEquals(entity.getChangePercentage(), response.getChangePercentage());
        assertEquals(entity.getPrice(), response.getPrice());
        assertEquals(entity.getTotalVol(),response.getTotalVol());
        assertEquals(entity.getMarket(), response.getMarket());
    }

    private Stock getStock() {
        Stock stock = new Stock();
        stock.setChangePoint(1.5);
        stock.setChangePercentage(1.7);
        stock.setTotalVol("23.0M");
        return stock;
    }

    private StockEntity getStockEntity() {
        StockEntity entity = new StockEntity();
        entity.setChangePoint(1.5);
        entity.setChangePercentage(1.7);
        entity.setTotalVol("23.0M");
        entity.setMarket("TSLA");
        return entity;
    }
}
