package com.gamesys.db.service.implementation;

import com.gamesys.BaseTest;
import com.gamesys.db.entity.StockEntity;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class StockRepositoryTest extends BaseTest {
    @Autowired
    StockRepository stockRepository;

    @Test
    public void testSaveSuccess() {
        // given
        StockEntity entity = getStockEntity();
        // when
        stockRepository.save(entity);
        // then
    }

    @Test
    public void testFindAllStockEntitiesSuccess() {
        // given
        StockEntity entity = getStockEntity();
        stockRepository.save(entity);
        // when
        List<StockEntity> stockEntities = stockRepository.findAllStockEntities(10, 0);
        // then
        assertNotNull(stockEntities);
        assertEquals(entity.getMarket(), stockEntities.get(0).getMarket());
    }

    @Test
    public void testFindAllStocksByMarketSuccess() {
        // given
        StockEntity entity = getStockEntity();
        stockRepository.save(entity);
        // when
        List<StockEntity> stockEntities = stockRepository.findAllStocksByMarket("TSLA", 10, 0);
        // then
        assertNotNull(stockEntities);
        assertEquals(entity.getMarket(), stockEntities.get(0).getMarket());
    }

    @Test
    public void testFindStocksByPriceSuccess() {
        // given
        Double price = 1.0;
        // when
        int result = stockRepository.findStocksByPrice(price);
        // then
        assertEquals(0, result);
    }

    private StockEntity getStockEntity() {
        StockEntity entity = new StockEntity();
        entity.setChangePoint(1.5);
        entity.setChangePercentage(2.03);
        entity.setTotalVol("23.0M");
        entity.setMarket("TSLA");
        entity.setPrice(1.1);
        return entity;
    }
}
