package com.gamesys.db.service.implementation;

import com.gamesys.BaseTest;
import com.gamesys.controller.model.StockResponse;
import com.gamesys.db.entity.StockEntity;
import com.gamesys.db.mapper.StockMapper;
import com.gamesys.domain.mapper.StocksResponseMapper;
import com.gamesys.external.model.Stock;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

public class DbTransactionServiceTest extends BaseTest {
    @Mock
    private StockRepository stockRepository;
    @Mock
    private StocksResponseMapper stocksResponseMapper;
    @Mock
    private StockMapper stockMapper;
    @InjectMocks
    private DbTransactionService dbTransactionService;

    @Test
    public void testGetStocksSuccess() {
        // given
        List<StockEntity> stocks = mock(ArrayList.class);
        List<StockResponse> stockResponses = getStockResponses();
        when(stockRepository.findAllStockEntities(Mockito.anyInt(), Mockito.anyInt())).thenReturn(stocks);
        when(stocksResponseMapper.map(Mockito.any())).thenReturn(stockResponses);
        // when
        List<StockResponse> response = dbTransactionService.getStocks();
        // then
        assertNotNull(response);

        assertNotNull(response);
        for (int i = 0; i < stocks.size(); i++) {
            assertEquals(stockResponses.get(i).getInsertDate(), response.get(i).getInsertDate());
            assertEquals(stockResponses.get(i).getChangePercentage(), response.get(i).getChangePercentage());
            assertEquals(stockResponses.get(i).getChangePoint(), response.get(i).getChangePoint());
            assertEquals(stockResponses.get(i).getPrice(), response.get(i).getPrice());
            assertEquals(stockResponses.get(i).getMarket(), response.get(i).getMarket());
        }
    }

    @Test
    public void testGetStocksByMarketSuccess() {
        // given
        List<StockEntity> stocks = mock(ArrayList.class);
        List<StockResponse> stockResponses = getStockResponses();
        when(stockRepository.findAllStockEntities(Mockito.anyInt(), Mockito.anyInt())).thenReturn(stocks);
        when(stocksResponseMapper.map(Mockito.any())).thenReturn(stockResponses);
        // when
        List<StockResponse> response = dbTransactionService.getStocksByMarket("TSLA");
        // then
        assertNotNull(response);

        assertNotNull(response);
        for (int i = 0; i < stocks.size(); i++) {
            assertEquals(stockResponses.get(i).getInsertDate(), response.get(i).getInsertDate());
            assertEquals(stockResponses.get(i).getChangePercentage(), response.get(i).getChangePercentage());
            assertEquals(stockResponses.get(i).getChangePoint(), response.get(i).getChangePoint());
            assertEquals(stockResponses.get(i).getPrice(), response.get(i).getPrice());
            assertEquals(stockResponses.get(i).getMarket(), response.get(i).getMarket());
        }
    }

    @Test
    public void testSaveSuccess() {
        // given
        Stock stock = getStock();
        StockEntity stockEntity = getStockEntity();
        String market = "TSLA";
        when(stockMapper.map(Mockito.any(), Mockito.anyString())).thenReturn(stockEntity);
        when(stockRepository.findStocksByPrice(Mockito.anyDouble())).thenReturn(0);
        doNothing().when(stockRepository).save(Mockito.isA(StockEntity.class));
        // when
        dbTransactionService.save(stock, market);
        // then
    }

    private List<StockResponse> getStockResponses() {
        List<StockResponse> stockResponses = new ArrayList<>();
        StockResponse stock = new StockResponse();
        stock.setPrice(1.0);
        stock.setTotalVol("23.0M");
        stock.setMarket("TSLA");
        stock.setChangePoint(1.0);
        stock.setChangePercentage(1.0);
        stockResponses.add(stock);
        return stockResponses;
    }

    private Stock getStock() {
        Stock stock = new Stock();
        stock.setChangePoint(1.5);
        stock.setChangePercentage(1.7);
        stock.setTotalVol("23.0M");
        return stock;
    }

    private StockEntity getStockEntity() {
        StockEntity entity = new StockEntity();
        entity.setChangePoint(1.5);
        entity.setChangePercentage(1.7);
        entity.setTotalVol("23.0M");
        entity.setMarket("TSLA");
        return entity;
    }
}
