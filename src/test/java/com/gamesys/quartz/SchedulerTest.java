package com.gamesys.quartz;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.gamesys.BaseTest;
import com.gamesys.db.service.implementation.DbTransactionService;
import com.gamesys.external.model.Stock;
import com.gamesys.external.service.implementation.StonksService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;

import static org.mockito.Mockito.*;

public class SchedulerTest extends BaseTest {
    private static final String STONKS_MARKET = "TSLA";

    @Mock
    private StonksService stonksService;

    @Mock
    private DbTransactionService transactionService;

    @InjectMocks
    private Scheduler scheduler;

    @Before
    public void before() {
        ReflectionTestUtils.setField(scheduler, "STONKS_MARKET", STONKS_MARKET);
    }

    @Test
    public void testCronJobSchSuccess() throws JsonProcessingException {
        // given
        Stock stock = mock(Stock.class);
        when(stonksService.stockRate(Mockito.anyString())).thenReturn(stock);
        doNothing().when(transactionService).save(Mockito.isA(Stock.class), Mockito.anyString());
        // when
        scheduler.cronJobSch();
    }

}
