package com.gamesys.controller;

import com.gamesys.BaseTest;
import com.gamesys.controller.model.ErrorResponse;
import com.gamesys.domain.exception.RealStonksServerError;
import org.junit.Test;
import org.mockito.InjectMocks;

import static org.junit.Assert.assertNotNull;


public class RestErrorHandlerTest extends BaseTest {

    @InjectMocks
    private RestErrorHandler restErrorHandler;

    @Test
    public void testHandleProviderErrorSuccess() {
        // when
        ErrorResponse error = restErrorHandler.handleProviderError(new RealStonksServerError("01", "error"));
        // then
        assertNotNull(error);
    }

}
