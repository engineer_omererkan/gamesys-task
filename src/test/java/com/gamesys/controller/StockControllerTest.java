package com.gamesys.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.gamesys.BaseTest;
import com.gamesys.controller.model.StockResponse;
import com.gamesys.domain.service.implementation.EntryService;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

public class StockControllerTest extends BaseTest {
    @Mock
    private EntryService entryService;

    @InjectMocks
    private StockController stockController;

    @Test
    public void testStocksSuccess() {
        // given
        List<StockResponse> stocks = new ArrayList<>();
        stocks.add(getStockResponse());
        when(entryService.getStocksRate()).thenReturn(stocks);
        // when
        List<StockResponse> stockResponseList = stockController.stocks();
        // then

        assertNotNull(stockResponseList);
        for (int i = 0; i < stocks.size(); i++) {
            assertEquals(stocks.get(i).getChangePercentage(), stockResponseList.get(i).getChangePercentage());
            assertEquals(stocks.get(i).getChangePoint(), stockResponseList.get(i).getChangePoint());
            assertEquals(stocks.get(i).getInsertDate(), stockResponseList.get(i).getInsertDate());
            assertEquals(stocks.get(i).getMarket(), stockResponseList.get(i).getMarket());
            assertEquals(stocks.get(i).getPrice(), stockResponseList.get(i).getPrice());
        }
    }

    @Test
    public void testMarketStocksSuccess() {
        // given
        List<StockResponse> stocks = new ArrayList<>();
        stocks.add(getStockResponse());
        when(entryService.getMarketStocksRate(Mockito.anyString())).thenReturn(stocks);
        // when
        List<StockResponse> stockResponseList = stockController.marketStocks("TSLA");
        // then

        assertNotNull(stockResponseList);
        for (int i = 0; i < stocks.size(); i++) {
            assertEquals(stocks.get(i).getInsertDate(), stockResponseList.get(i).getInsertDate());
            assertEquals(stocks.get(i).getChangePercentage(), stockResponseList.get(i).getChangePercentage());
            assertEquals(stocks.get(i).getChangePoint(), stockResponseList.get(i).getChangePoint());
            assertEquals(stocks.get(i).getPrice(), stockResponseList.get(i).getPrice());
            assertEquals(stocks.get(i).getMarket(), stockResponseList.get(i).getMarket());
        }
    }

    @Test
    public void testMarketStockSuccess() throws JsonProcessingException {
        // given
        StockResponse stock = getStockResponse();
        when(entryService.stockRate(Mockito.anyString())).thenReturn(stock);
        // when
        StockResponse stockResponse = stockController.marketStock("TSLA");
        // then
        assertNotNull(stockResponse);
        assertEquals(stock.getPrice(), stockResponse.getPrice());
        assertEquals(stock.getMarket(), stockResponse.getMarket());
        assertEquals(stock.getChangePoint(), stockResponse.getChangePoint());
        assertEquals(stock.getInsertDate(), stockResponse.getInsertDate());
        assertEquals(stock.getChangePercentage(), stockResponse.getChangePercentage());
        assertEquals(stock.getTotalVol(), stockResponse.getTotalVol());

    }


    private StockResponse getStockResponse() {
        StockResponse stockResponse = new StockResponse();
        stockResponse.setChangePoint(1.5);
        stockResponse.setChangePercentage(1.7);
        stockResponse.setMarket("TSLA");
        stockResponse.setTotalVol("23.0M");
        return stockResponse;
    }
}
