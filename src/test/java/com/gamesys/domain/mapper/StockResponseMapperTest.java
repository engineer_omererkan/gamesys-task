package com.gamesys.domain.mapper;

import com.gamesys.BaseTest;
import com.gamesys.controller.model.StockResponse;
import com.gamesys.external.model.Stock;
import org.junit.Test;
import org.mockito.InjectMocks;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class StockResponseMapperTest extends BaseTest {

    private final Stock stock;
    private final StockResponse stockResponse;

    @InjectMocks
    private StockResponseMapper mapper;

    public StockResponseMapperTest() {
        stock = getStock();
        stockResponse = getStockResponse();
    }


    @Test
    public void testMapperSuccess() {
        // given
        String market = "TSLA";
        // when
        StockResponse response = mapper.map(stock, market);
        // then
        assertNotNull(response);
        assertEquals(stockResponse.getTotalVol(), response.getTotalVol());
        assertEquals(stockResponse.getChangePercentage(), response.getChangePercentage());
        assertEquals(stockResponse.getChangePoint(), response.getChangePoint());
        assertEquals(stockResponse.getPrice(), response.getPrice());
        assertEquals(stockResponse.getTotalVol(), response.getTotalVol());
        assertEquals(stockResponse.getMarket(), response.getMarket());
    }

    private StockResponse getStockResponse() {
        StockResponse stockResponse = new StockResponse();
        stockResponse.setChangePercentage(1.5);
        stockResponse.setChangePoint(10.05);
        stockResponse.setPrice(681.02);
        stockResponse.setTotalVol("29.43M");
        stockResponse.setMarket("TSLA");
        return stockResponse;
    }

    private Stock getStock() {
        Stock stock = new Stock();
        stock.setChangePercentage(1.5);
        stock.setChangePoint(10.05);
        stock.setPrice(681.02);
        stock.setTotalVol("29.43M");
        return stock;
    }
}
