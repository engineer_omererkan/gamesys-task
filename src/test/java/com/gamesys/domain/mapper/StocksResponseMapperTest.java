package com.gamesys.domain.mapper;

import com.gamesys.BaseTest;
import com.gamesys.controller.model.StockResponse;
import com.gamesys.db.entity.StockEntity;
import org.junit.Test;
import org.mockito.InjectMocks;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class StocksResponseMapperTest extends BaseTest {
    private final List<StockEntity> stockEntities;

    private final List<StockResponse> stockResponses;

    @InjectMocks
    private StocksResponseMapper mapper;

    public StocksResponseMapperTest() {
        stockEntities = geStockEntities();
        stockResponses = getStockResponses();
    }

    @Test
    public void testMapperSuccess() {
        // when
        List<StockResponse> responseList = mapper.map(stockEntities);
        // then
        assertNotNull(responseList);
        for (int i = 0; i < stockResponses.size(); i++) {
            assertEquals(stockResponses.get(i).getMarket(), responseList.get(i).getMarket());
            assertEquals(stockResponses.get(i).getTotalVol(), responseList.get(i).getTotalVol());
            assertEquals(stockResponses.get(i).getPrice(), responseList.get(i).getPrice());
            assertEquals(stockResponses.get(i).getChangePoint(), responseList.get(i).getChangePoint());
            assertEquals(stockResponses.get(i).getChangePercentage(), responseList.get(i).getChangePercentage());

        }
    }

    private List<StockResponse> getStockResponses() {
        List<StockResponse> stockResponses = new ArrayList<>();
        StockResponse stock = new StockResponse();
        stock.setPrice(1.0);
        stock.setTotalVol("23.0M");
        stock.setMarket("TSLA");
        stock.setChangePoint(1.0);
        stock.setChangePercentage(1.0);
        stockResponses.add(stock);
        return stockResponses;
    }

    private List<StockEntity> geStockEntities() {
        List<StockEntity> stockEntities = new ArrayList<>();
        StockEntity entity = new StockEntity();
        entity.setPrice(1.0);
        entity.setTotalVol("23.0M");
        entity.setMarket("TSLA");
        entity.setChangePoint(1.0);
        entity.setChangePercentage(1.0);
        stockEntities.add(entity);
        return stockEntities;
    }
}
