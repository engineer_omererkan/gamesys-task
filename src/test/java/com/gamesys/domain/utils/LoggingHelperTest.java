package com.gamesys.domain.utils;

import com.gamesys.BaseTest;
import com.gamesys.domain.enumerator.Error;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class LoggingHelperTest extends BaseTest {
    @Test
    public void testPrepareErrorLog() {
        // given
        String errorCode = Error.JSON_PARSE_EXCEPTION.getCode();
        String errorMessage = Error.JSON_PARSE_EXCEPTION.getMessage();
        String errorId = "3575f48e-015a-43a6-8d2c-1a98cbfb64fa";
        String errorStack = "error";
        // when
        String loggerMessage = LoggingHelper.prepareErrorLog(errorCode, errorMessage, errorId, errorStack);
        // then
        assertNotNull(loggerMessage);
        assertEquals("Mail Provider Exception: 3575f48e-015a-43a6-8d2c-1a98cbfb64fa \n" +
                "Code: 02 \n" +
                "Message:Json Parse Exception \n" +
                "ErrorStack:3575f48e-015a-43a6-8d2c-1a98cbfb64fa", loggerMessage);
    }
}