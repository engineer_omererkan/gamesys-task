package com.gamesys.domain.service.implementation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.gamesys.BaseTest;
import com.gamesys.controller.model.StockResponse;
import com.gamesys.db.service.implementation.DbTransactionService;
import com.gamesys.domain.mapper.StockResponseMapper;
import com.gamesys.external.model.Stock;
import com.gamesys.external.service.implementation.StonksService;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class EntryServiceTest extends BaseTest {

    @Mock
    private StonksService stockRateService;
    @Mock
    private StockResponseMapper stockResponseMapper;
    @Mock
    private DbTransactionService stockRateTransactionService;

    @InjectMocks
    private EntryService entryService;


    @Test
    public void testGetStocksRateSuccess() {
        // given
        List<StockResponse> stockResponses = new ArrayList<>();
        stockResponses.add(getStockResponse());
        when(stockRateTransactionService.getStocks()).thenReturn(stockResponses);
        // when
        List<StockResponse> stocksRate = entryService.getStocksRate();
        // then
        assertNotNull(stocksRate);
    }

    @Test
    public void testGetMarketStocksRateSuccess() {
        // given
        List<StockResponse> stockResponses = new ArrayList<>();
        stockResponses.add(getStockResponse());
        when(stockRateTransactionService.getStocksByMarket(Mockito.anyString())).thenReturn(stockResponses);
        // when
        List<StockResponse> stocksRate = entryService.getMarketStocksRate("TSLA");
        // then
        assertNotNull(stocksRate);
    }

    @Test
    public void testStockRateSuccess() throws JsonProcessingException {
        // given
        Stock stock = mock(Stock.class);
        StockResponse stockResponse = getStockResponse();
        when(stockRateService.stockRate(Mockito.anyString())).thenReturn(stock);
        when(stockResponseMapper.map(Mockito.isA(Stock.class), Mockito.anyString())).thenReturn(stockResponse);
        // when
        StockResponse response = entryService.stockRate("TSLA");
        // then
        assertNotNull(response);
        assertEquals(stockResponse.getChangePercentage(), response.getChangePercentage());
        assertEquals(stockResponse.getPrice(), response.getPrice());
        assertEquals(stockResponse.getChangePoint(), response.getChangePoint());
        assertEquals(stockResponse.getMarket(), response.getMarket());
        assertEquals(stockResponse.getTotalVol(), response.getTotalVol());
        assertEquals(stockResponse.getInsertDate(), response.getInsertDate());
    }

    private StockResponse getStockResponse() {
        StockResponse stockResponse = new StockResponse();
        stockResponse.setChangePercentage(1.5);
        stockResponse.setChangePoint(10.05);
        stockResponse.setPrice(681.02);
        stockResponse.setTotalVol("29.43M");
        stockResponse.setMarket("TSLA");
        stockResponse.setInsertDate(new Date());
        return stockResponse;
    }

}
