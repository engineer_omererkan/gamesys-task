# Introduction 
Gamesys test task. 
This project calls RealStonks api for getting Realtime Stonk information according to market base

# Getting Started
TODO: Guide users through getting your code up and running on their own system. In this section you can talk about:
1.	Installation process
      * Java 11
      * IDE (IntelliJ or Eclipse or another IDE)
      * Run project as Spring Boot application.
2.	Software dependencies
      * Spring Boot 2.4.1
      * Spring Boot Jdbc
      * H2 in-memory db 
      * Junit
3.	API references
        * http://localhost:8080/api/all/stocks (Http GET Method) : it's getting last then record from db
        * http://localhost:8080/api/market/stocks?market=TSLA (Http GET Method) : it's getting last then record according to market from
        * http://localhost:8080/api/stock/rate?market=TSLA (Http GET Method) : it calls external API (https://realstonks.p.rapidapi.com) for realtime stock rate information
# Build and Test
        * Run project as Springboot application. (Class is src/main/java/com.gamesys/App.class)
        * Run Unit tests and db integration test

# Project Details
a.       Read input data from any external http source (for you to choose, rss feed, twitter feed...)  every x seconds-minutes
        * I choose  RealStonk api for that project because I want to take data as realtime. Meanwhile, even it's realtime api sometimes it retrieves same data. 
            In fact to retrieve same data, I insert some static data to db. So it can retrieve more data like the last ten record.
        * Scheduler is used in that project for calling external api. It's a cron job and run every 30 seconds.
        * This project doesn't allow duplicate values according to price. (Price is unique)

b.       Process data with any rules (modify input value, add additional values)
        * Apart from data from RealStonk api, I insert market and insertData fields to db. And for simple data manipulation, calculationPoints is multiply with 2. It's just for changing input.
            

c.       Put it in any in-memory database (H2 for example)
        * H2 is used as in-memory db

d.       By the request in browser show the last 10 entries (no UI, just json) by reading from the in-memory db
        * it retrieves last ten 10 records
e.       Don’t use hibernate (JPA). Use plain SQL.
        * Jdbc is used in that project
f.        Spring Boot 2, Java
        * Spring Boot 2 is used
        * Java 11 is used

g.       No Spring integrations framework
        * Spring integrations framework is not used 

h.       (optional) Create a Docker image for the project, so we can run this as a Docker container
        * I have problem with computer to install Docker in Windows That's why I didn't take risk of not testing Dockerfile.
         However, I prepared a Docker file like below. Just, because I couldn't test it. Please don't evaluate it if it's not work.

1) Framework for tests: junit
    * Junit and Mockito is used for tests. Current test coverage is almost %90 percentage

2) Please, do not spend time on complex processing logic.
    * I focus on architecture of project rather than a complex logic.
3) Run tests
   
#Sample Dockerfile

FROM adoptopenjdk/openjdk11:alpine-jre
RUN addgroup -S spring && adduser -S spring -G spring
USER spring:spring
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
